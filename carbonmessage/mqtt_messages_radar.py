import socket

from .mqtt.simple_subscriber import SimpleSubscriber
from .mqtt.simple_publisher import SimplePublisher


class MQTTMessagesRadar():
    def __init__(self, logger, broker):
        self.logger = logger
        self.broker = broker

    def subscribe_to_topics(self):
        simple = SimpleSubscriber(self.logger, self.broker)
        try:
            simple.subscribe_and_wait_messages()
        except socket.gaierror as e_serious:
            self.logger.error("During Subscribe an Exception is Raised ... %s",
                              e_serious)

    def publish_to_topics(self):
        simple = SimplePublisher(self.logger, self.broker)
        try:
            simple.publish_single_message_to_topic()
        except socket.gaierror as e_serious:
            self.logger.error("During Publish an Exception is Raised ... %s",
                              e_serious)

        #simple.publish_multiple_messages_to_topic()
