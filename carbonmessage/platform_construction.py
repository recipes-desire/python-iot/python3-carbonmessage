from .utils.external_configuration import read_config
from .utils.local_logger import LocalLogger


class PlatformConstruction():
    def __init__(self, conf_filename="/etc/carbonmessage/carbonmessage.conf"):
        self.conf_filename = conf_filename

        self._logger = None

        self._default_section = None
        self._local_broker = None
        self._central_broker = None

    def apply_external_configuration(self):
        #print("Ready to Read External Configuration ...")

        [default_section, local_broker_data,
         central_broker_data] = read_config(self.conf_filename)

        self._default_section = default_section
        self._local_broker = local_broker_data
        self._central_broker = central_broker_data

    def prepare_logger(self):
        logger_local = LocalLogger(self.default_section.log_filename)

        logger_local.level = self.default_section.log_level
        logger_local.msg_format = self.default_section.msg_format
        logger_local.max_bytes = self.default_section.max_bytes
        logger_local.backup_count = self.default_section.backup_count

        self._logger = logger_local.log_setup()

    def turn_key_solution(self):
        #print("Ready to Start MQTT Platform Construction !")
        self.apply_external_configuration()

        self.prepare_logger()

    def debug(self, message):
        self._logger.debug(message)

    def error(self, message):
        self._logger.error(message)

    def info(self, message):
        self._logger.info(message)

    @property
    def local_broker(self):
        return self._local_broker

    @property
    def central_broker(self):
        return self._central_broker

    @property
    def default_section(self):
        return self._default_section

    @property
    def logger(self):
        return self._logger
