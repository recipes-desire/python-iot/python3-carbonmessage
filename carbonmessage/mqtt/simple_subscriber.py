import paho.mqtt.subscribe as subscribe


class SimpleSubscriber():
    def __init__(self, logger, broker):
        self.logger = logger

        self.topics = broker.topics
        self.broker = broker

    def subscribe_to_single_topic(self, tindex=0, msg_count=1):
        my_topic = self.topics[tindex]

        messages = subscribe.simple(my_topic,
                                    hostname=self.broker.host,
                                    port=self.broker.port,
                                    retained=False,
                                    msg_count=msg_count)

        for msg in messages:
            self.logger.debug("New Messaged Received on Topic: %s/ %s",
                              msg.topic, msg.payload)

    def subscribe_and_wait_messages(self, msg_count=1):
        messages = subscribe.simple(self.topics,
                                    hostname=self.broker.host,
                                    port=self.broker.port,
                                    retained=False,
                                    msg_count=msg_count)

        for msg in messages:
            self.logger.debug("New Message Received: %s/ %s", msg.topic,
                              msg.payload)
