class DefaultSectionData():
    def __init__(
            self,
            log_filename="/var/log/carbonmessage.conf",
            log_level="DEBUG",
    ):
        self._is_daemon = None
        self._pid = None

        self._log_filename = log_filename
        self._log_level = log_level

        self._msg_format = None
        self._max_bytes = None
        self._backup_count = None

    @property
    def is_daemon(self):
        return self._is_daemon

    @is_daemon.setter
    def is_daemon(self, value):
        self._is_daemon = value

    @property
    def pid(self):
        return self._pid

    @pid.setter
    def pid(self, value):
        self._pid = value

    @property
    def log_filename(self):
        return self._log_filename

    @log_filename.setter
    def log_filename(self, value):
        self._log_filename = value

    @property
    def log_level(self):
        return self._log_level

    @log_level.setter
    def log_level(self, value):
        self._log_level = value

    @property
    def msg_format(self):
        return self._msg_format

    @msg_format.setter
    def msg_format(self, value):
        self._msg_format = value

    @property
    def max_bytes(self):
        return self._max_bytes

    @max_bytes.setter
    def max_bytes(self, value):
        self._max_bytes = value

    @property
    def backup_count(self):
        return self._backup_count

    @backup_count.setter
    def backup_count(self, value):
        self._backup_count = value
