from configparser import ConfigParser, ExtendedInterpolation

from .default_section_data import DefaultSectionData
from .broker_data import BrokerData


def read_default_section(config):
    # Is-Daemon ?
    is_daemon = config.getboolean("default", "is-daemon")
    pid = config.get("default", "pid")

    # Logging
    log_filename = config.get("default", "log-filename")
    log_level = config.get("default", "log-level")
    msg_format = config.get("default", "msg-format")
    max_bytes = config.getint("default", "max-bytes")
    backup_count = config.getint("default", "backup-count")

    default_section = DefaultSectionData(log_filename=log_filename,
                                         log_level=log_level)
    default_section.is_daemon = is_daemon
    default_section.pid = pid

    default_section.msg_format = msg_format
    default_section.max_bytes = max_bytes
    default_section.backup_count = backup_count

    return default_section


def read_config(file_name):
    """
    Read external configuration file and assign data to local variables.
    """
    config = ConfigParser(interpolation=ExtendedInterpolation())

    response = config.read(file_name)
    if not response:
        # Nothing read .. aborting
        raise Exception('Wrong Filename ==> Nothing Read & Aborting !')

    default_section = read_default_section(config)

    # Broker - Local
    is_available = config.getboolean("local-broker", "is-available")

    host = config.get("local-broker", "host")
    port = config.getint("local-broker", "port")

    topics = config.get("local-broker", "topics")
    _topics = topics.split(",")
    t_opics = []

    #print("Topics array is ... {}".format(_topics))
    for topic in _topics:
        # Topics without trailing and leading spaces
        t_opics.append(topic.strip())
    #print("Topics Array is .... {}".format(t_opics))

    local_broker_data = BrokerData(port=port, host=host, topics=t_opics)
    local_broker_data.is_local = True
    local_broker_data.is_available = is_available

    # Broker - Central
    is_available = config.getboolean("central-broker", "is-available")

    host = config.get("central-broker", "host")
    port = config.getint("central-broker", "port")

    central_broker_data = BrokerData(port=port, host=host)

    central_broker_data.is_local = True
    central_broker_data.is_available = is_available

    return [default_section, local_broker_data, central_broker_data]
