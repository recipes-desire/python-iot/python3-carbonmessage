import logging
from logging.handlers import RotatingFileHandler

LEVELS = {
    "CRITICAL": logging.CRITICAL,
    "ERROR": logging.ERROR,
    "WARNING": logging.WARNING,
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG,
}


class LocalLogger():
    def __init__(self, filename):
        self.filename = filename

        self.level = None
        self.max_bytes = None
        self.backup_count = None
        self.msg_format = None

        self._logger = None

    def log_setup(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(LEVELS[self.level])

        # sudo chmod 777 /var/log/nmapwrapper/nmapwrapper.log
        file_handler = RotatingFileHandler(filename=self.filename,
                                           maxBytes=self.max_bytes * 1024 *
                                           1024,
                                           backupCount=self.backup_count)

        formatter = logging.Formatter(self.msg_format)
        file_handler.setFormatter(formatter)

        logger.addHandler(file_handler)

        logger.propagate = False

        return logger

    def get_level(self):
        return self.level

    def get_msg_format(self):
        return self.msg_format
