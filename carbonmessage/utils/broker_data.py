class BrokerData():
    """
    Necessary data to establish communication with an MQTT-Broker for IoT messages.
    """

    def __init__(self, host, port=83339, topics="#"):
        self._is_local = None
        self._is_available = None

        self._host = host
        self._port = port

        self._topics = topics

    @property
    def is_local(self):
        return self._is_local

    @is_local.setter
    def is_local(self, value):
        self._is_local = value

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def is_available(self):
        return self._is_available

    @is_available.setter
    def is_available(self, value):
        self._is_available = value

    @property
    def topics(self):
        return self._topics
