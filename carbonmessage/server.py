from .platform_construction import PlatformConstruction
from .mqtt_messages_radar import MQTTMessagesRadar


def preparation():
    # Read external configuration & setup various supporting modules
    platform = PlatformConstruction()
    platform.turn_key_solution()

    radar = MQTTMessagesRadar(platform.logger, platform.local_broker)

    return radar


# Application entrypoint to start server prodedure ... Scan all mqtt-brokers for new messages
def main_subscribe():
    radar = preparation()
    radar.subscribe_to_topics()


def main_publish():
    radar = preparation()
    radar.publish_to_topics()
